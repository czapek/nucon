#pragma once

#include <base/types.h>

void memory_copy(u8 *destination, const u8 *source, usize count);
void memory_set(u8 *destination, u8 value, usize count);
