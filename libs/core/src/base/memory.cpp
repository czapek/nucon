#include <base/memory.h>

static constexpr u64 explode_byte(u8 byte) {
    auto value = (u64) byte;

    return value << 56 | value << 48 | value << 40 | value << 32 | value << 24 | value << 16 | value << 8 | value;
}

void memory_copy(u8 *destination, const u8 *source, usize count) {
    if (count >= 8) {
        auto count_q = count / sizeof(u64);

        asm volatile("rep movsq" : "=S"(source), "=D"(destination) : "S"(source), "D"(destination), "c"(count_q));

        count -= count_q * sizeof(u64);

        if (count == 0)
            return;
    }

    asm volatile("rep movsb" : : "S"(source), "D"(destination), "c"(count));
}

void memory_set(u8 *destination, u8 value, usize count) {
    if (count >= 8) {
        auto count_q = count / sizeof(u64);

        asm volatile("rep stosq" : "=D"(destination) : "D"(destination), "c"(count_q), "a"(explode_byte(value)));

        count -= count_q * sizeof(u64);

        if (count == 0)
            return;
    }

    asm volatile("rep stosb" : "=D"(destination), "=c"(count) : "0"(destination), "1"(count), "a"(value));
}
