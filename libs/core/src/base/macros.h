#pragma once

#define __MAKE_ENUM_OPERATOR(name, op)                                                                                          \
    constexpr inline name operator op(name a, name b) { return static_cast<name>(static_cast<u64>(a) op static_cast<u64>(b)); } \
    constexpr inline name operator op(name a, u64 b) { return static_cast<name>(static_cast<u64>(a) op b); }

#define MAKE_ENUM_OPERATORS(name)                                                               \
    constexpr inline name operator~(name a) { return static_cast<name>(~static_cast<u64>(a)); } \
    __MAKE_ENUM_OPERATOR(name, |)                                                               \
    __MAKE_ENUM_OPERATOR(name, &)                                                               \
    __MAKE_ENUM_OPERATOR(name, ^)                                                               \
    __MAKE_ENUM_OPERATOR(name, <<)                                                              \
    __MAKE_ENUM_OPERATOR(name, >>)                                                              \
    __MAKE_ENUM_OPERATOR(name, +)
