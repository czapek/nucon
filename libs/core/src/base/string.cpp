#include <base/string.h>

usize string_length(const char* str) {
    auto length = 0u;

    while (*str++) {
        length++;
    }

    return length;
}

usize string_compare(const char* str1, const char* str2, usize max_length) {
    while (*str1 && *str2 && max_length) {
        if (*str1 != *str2)
            return *str1 - *str2;

        str1++;
        str2++;

        max_length--;
    }

    return 0;
}

usize string_find(const char* str, char ch) {
    auto index = 0u;

    while (str[index]) {
        if (str[index] == ch)
            return index;

        index++;
    }

    return -1;
}

usize string_find_last(const char* str, char ch) {
    auto index = string_length(str) - 1;

    while (str[index]) {
        if (str[index] == ch)
            return index;

        index++;
    }

    return -1;
}
