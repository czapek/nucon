#pragma once

#include <base/types.h>

usize string_length(const char* str);
usize string_compare(const char* str1, const char* str2, usize max_length = -1);
usize string_find(const char* str, char ch);
usize string_find_last(const char* str, char ch);
