#pragma once

#define ASSERT_MSG(expr, message)
#define ASSERT(expr) ASSERT_MSG(expr, #expr)
