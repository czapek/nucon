#pragma once

#include <base/assert.h>
#include <base/types.h>
#include <core/initializer_list.h>

template <typename T>
class Span {
public:
    using Type = T;
    using Iterator = Type*;             // Iterator<Type>;
    using ConstIterator = const Type*;  // Iterator<const Type>;

    constexpr Span() = default;

    constexpr Span(Type* data, usize size) : m_data(data), m_size(size) {}

    template <usize N>
    constexpr Span(Type (&data)[N]) : m_data(data), m_size(N) {}

    constexpr Type& operator[](usize index) {
        ASSERT(index < m_size);
        return m_data[index];
    }

    constexpr const Type& operator[](usize index) const {
        ASSERT(index < m_size);
        return m_data[index];
    }

    constexpr Type* data() { return m_data; }
    constexpr const Type* data() const { return m_data; }

    constexpr usize size() const { return m_size; }

    constexpr Iterator begin() { return m_data; }
    constexpr Iterator end() { return m_data + m_size; }

    constexpr ConstIterator begin() const { return m_data; }
    constexpr ConstIterator end() const { return m_data + m_size; }

private:
    Type* m_data;
    usize m_size;
};
