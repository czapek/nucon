#pragma once

#include <base/assert.h>
#include <base/types.h>
#include <core/initializer_list.h>
#include <core/type_traits.h>

template <typename T, usize N>
class Array {
public:
    using Type = T;

    using Iterator = Type*;
    using ConstIterator = const Type*;

    constexpr T* data() { return __elements; }
    constexpr const T* data() const { return __elements; }

    constexpr Type& operator[](usize index) {
        ASSERT_MSG(index < N, "tried to index out of array bounds");
        return __elements[index];
    }

    constexpr const Type& operator[](usize index) const {
        ASSERT_MSG(index < N, "tried to index out of array bounds");
        return __elements[index];
    }

    constexpr usize size() const { return N; }

    constexpr Iterator begin() { return __elements; }
    constexpr Iterator end() { return __elements + N; }

    constexpr ConstIterator begin() const { return __elements; }
    constexpr ConstIterator end() const { return __elements + N; }

    T __elements[N];
};

template <typename T, IsConvertible<T>... Args>
Array(T, Args...) -> Array<T, sizeof...(Args) + 1>;
