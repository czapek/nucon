#pragma once

template <typename From, typename To>
concept IsConvertible = __is_convertible_to(From, To);

template <typename T, typename U>
concept IsSame = __is_same(T, U);

template <typename T>
concept HasTrivialAssign = __has_trivial_assign(T);
template <typename T>
concept HasTrivialCtor = __has_trivial_constructor(T);
template <typename T>
concept HasTrivialCopy = __has_trivial_copy(T);
template <typename T>
concept HasTrivialDtor = __has_trivial_destructor(T);
template <typename T>
concept HasVirtualDtor = __has_virtual_destructor(T);
template <typename T>
concept IsAbstract = __is_abstract(T);
template <typename T, typename U>
concept IsBaseOf = __is_base_of(T, U);
template <typename T>
concept IsClass = __is_class(T);
template <typename T>
concept IsEmpty = __is_empty(T);
template <typename T>
concept IsEnum = __is_enum(T);
template <typename T>
concept IsPod = __is_pod(T);
template <typename T>
concept IsPolymorphic = __is_polymorphic(T);
template <typename T>
concept IsUnion = __is_union(T);
template <typename T>
concept IsTrivial = __is_trivial(T);
template <typename T>
concept IsStandardLayout = __is_standard_layout(T);
template <typename T>
concept IsTrivialCopyable = __is_trivially_copyable(T);
template <typename T>
concept IsLiteral = __is_literal_type(T);
template <typename T>
concept HasTrivialMoveCtor = __has_trivial_move_constructor(T);
template <typename T>
concept HasTrivialMoveAssign = __has_trivial_move_assign(T);
template <typename T>
concept IsConstructible = __is_constructible(T);
template <typename T>
concept IsTrivialConstructible = __is_trivially_constructible(T);
template <typename T>
concept IsTriviallyDestructible = __is_trivially_destructible(T);
template <typename T, typename U>
concept IsTriviallyAssignable = __is_trivially_assignable(T, U);

// template <typename T>
// concept HasAssign = __has_assign(T);
// template <typename T>
// concept HasCopy = __has_copy(T);
// template <typename T, typename U>
// concept IsUnderlying = __underlying_type(T, U);
// template <typename T>
// concept IsDestructible = __is_destructible(T);

namespace detail {

template <typename T>
struct RemoveReference {
    using Type = T;
};

template <typename T>
struct RemoveReference<T&> {
    using Type = T;
};

template <typename T>
struct RemoveReference<T&&> {
    using Type = T;
};

template <typename T>
struct RemovePointer {
    using Type = T;
};

template <typename T>
struct RemovePointer<T*> {
    using Type = typename RemovePointer<T>::Type;
};

template <typename T>
struct RemoveConst {
    using Type = T;
};

template <typename T>
struct RemoveConst<const T> {
    using Type = T;
};

template <typename T>
struct RemoveVolatile {
    using Type = T;
};

template <typename T>
struct RemoveVolatile<volatile T> {
    using Type = T;
};

}  // namespace detail

template <typename T>
using RemoveReference = typename detail::RemoveReference<T>::Type;

template <typename T>
using RemovePointer = typename detail::RemovePointer<T>::Type;

template <typename T>
using RemoveConst = typename detail::RemoveConst<T>::Type;

template <typename T>
using RemoveVolatile = typename detail::RemoveVolatile<T>::Type;

template <typename T>
using RemovePostFixes = RemovePointer<RemoveReference<T>>;

template <typename T>
using RemovePreFixes = RemoveConst<RemoveVolatile<T>>;

template <typename T>
using RemoveDecorators = RemovePreFixes<RemovePostFixes<T>>;
