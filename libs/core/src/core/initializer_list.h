#pragma once

namespace std {

template <typename T>
class initializer_list {
public:
    using Type = T;

    using Iterator = const T*;
    using ConstIterator = const T*;

    constexpr initializer_list() = default;
    constexpr initializer_list(const Type* first, const Type* last) : m_first(first), m_last(last) {}

    constexpr usize size() const { return static_cast<usize>(m_last - m_first); }

    constexpr ConstIterator begin() const { return m_first; }
    constexpr ConstIterator end() const { return m_last; }

private:
    const Type* m_first;
    const Type* m_last;
};

}  // namespace std

template <typename T>
using InitializerList = std::initializer_list<T>;
