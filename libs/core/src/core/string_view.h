#pragma once

#include <base/string.h>
#include <core/span.h>

class StringView : public Span<const char> {
public:
    using Span::Span;

    constexpr StringView(const char *str) : Span(str, string_length(str)) {}
};
