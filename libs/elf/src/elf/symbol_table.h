#pragma once

#include <base/types.h>

struct ElfSymbolEntry64 {
    u32 name;
    u8 info;
    u8 other;
    u16 section_index;
    u64 value;
    u64 size;
};
