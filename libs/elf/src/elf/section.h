#pragma once

#include <base/types.h>

enum class ElfSectionType : u32
{
    NULL = 0,
    PROG_BITS = 1,
    SYM_TAB = 2,
    STR_TAB = 3,
    RELA = 4,
    HASH = 5,
    DYNAMIC = 6,
    NOTE = 7,
    NO_BITS = 8,
    REL = 9,
    SH_LIB = 10,
    DYN_SYM = 11,
    INIT_ARRAY = 14,
    FINI_ARRAY = 15,
    PRE_INIT_ARRAY = 16,
    GROUP = 17,
    SYM_TAB_SH_INDEX = 18,
};

struct ElfSectionHeader64 {
    u32 name;
    u32 type;
    u64 flags;
    u64 address;
    u64 offset;
    u64 size;
    u32 link;
    u32 info;
    u64 address_align;
    u64 entry_size;
};
