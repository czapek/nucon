#include <gfx/surface.h>

static usize get_pixel_size(GfxPixelFormat format) {
    switch (format) {
        case GfxPixelFormat::UNKNOWN:
            return 0;
        case GfxPixelFormat::GRAYSCALE_8:
            return 1;
        case GfxPixelFormat::RGB_888:
            return 3;
        case GfxPixelFormat::RGBA_8888:
        case GfxPixelFormat::BGRA_8888:
            return 4;
    }

    return -1;
}

void GfxSurface::clear() {
    fill(GfxColor::BLACK);
}

void GfxSurface::fill(const GfxColor& color) {
    for (auto y = 0u; y < m_height; y++) {
        for (auto x = 0u; x < m_width; x++) {
            store(x, y, color);
        }
    }
}

void GfxSurface::store(usize x, usize y, const GfxColor& color) {
    if (x < 0 || y < 0 || x >= m_width || y >= m_height)
        return;

    auto pixel_data = (u8*) m_address + y * m_pitch + x * get_pixel_size(m_format);

    switch (m_format) {
        case GfxPixelFormat::UNKNOWN:
            break;
        case GfxPixelFormat::GRAYSCALE_8: {
            auto pixel = (GfxPixelGrayscale8*) pixel_data;

            pixel->value = (color.r() + color.g() + color.b()) / 3;
        }; break;
        case GfxPixelFormat::RGB_888: {
            auto pixel = (GfxPixelRgb888*) pixel_data;

            pixel->r = color.r();
            pixel->g = color.g();
            pixel->b = color.b();
        }; break;
        case GfxPixelFormat::RGBA_8888: {
            auto pixel = (GfxPixelRgba8888*) pixel_data;

            pixel->r = color.r();
            pixel->g = color.g();
            pixel->b = color.b();
        }; break;
        case GfxPixelFormat::BGRA_8888: {
            auto pixel = (GfxPixelBgra8888*) pixel_data;

            pixel->r = color.r();
            pixel->g = color.g();
            pixel->b = color.b();
        }; break;
    }
}

void GfxSurface::blit(usize x, usize y, const GfxSurface& other) {
    for (auto yy = 0u; yy < other.height(); yy++) {
        for (auto xx = 0u; xx < other.width(); xx++) {
            store(x + xx, y + yy, other.load(xx, yy));
        }
    }
}

GfxColor GfxSurface::load(usize x, usize y) const {
    if (x < 0 || y < 0 || x >= m_width || y >= m_height)
        return GfxColor::BLACK;

    auto pixel_data = (u8*) m_address + y * m_pitch + x * get_pixel_size(m_format);

    switch (m_format) {
        case GfxPixelFormat::UNKNOWN:
            return GfxColor::BLACK;
        case GfxPixelFormat::GRAYSCALE_8: {
            auto pixel = (GfxPixelGrayscale8*) pixel_data;

            return {pixel->value, pixel->value, pixel->value};
        } break;
        case GfxPixelFormat::RGB_888: {
            auto pixel = (GfxPixelRgb888*) pixel_data;

            return {pixel->r, pixel->g, pixel->b};
        } break;
        case GfxPixelFormat::RGBA_8888: {
            auto pixel = (GfxPixelRgba8888*) pixel_data;

            return {pixel->r, pixel->g, pixel->b};
        } break;
        case GfxPixelFormat::BGRA_8888: {
            auto pixel = (GfxPixelBgra8888*) pixel_data;

            return {pixel->r, pixel->g, pixel->b};
        } break;
    }

    return GfxColor::BLACK;
}
