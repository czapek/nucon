#pragma once

#include <base/types.h>

class GfxColor {
public:
    constexpr GfxColor() = default;
    constexpr GfxColor(u8 r, u8 g, u8 b, u8 a = 255) : m_r{r}, m_g{g}, m_b{b}, m_a{a} {}

    constexpr u8 r() const { return m_r; }
    constexpr u8 g() const { return m_g; }
    constexpr u8 b() const { return m_b; }
    constexpr u8 a() const { return m_a; }

    constexpr void set_r(u8 r) { m_r = r; }
    constexpr void set_g(u8 g) { m_g = g; }
    constexpr void set_b(u8 b) { m_b = b; }
    constexpr void set_a(u8 a) { m_a = a; }

    constexpr GfxColor with_r(u8 r) const { return {r, m_g, m_b, m_a}; }
    constexpr GfxColor with_g(u8 g) const { return {m_r, g, m_b, m_a}; }
    constexpr GfxColor with_b(u8 b) const { return {m_r, m_g, b, m_a}; }
    constexpr GfxColor with_a(u8 a) const { return {m_r, m_g, m_b, a}; }

    static const GfxColor TRANSPARENT;
    static const GfxColor BLACK;
    static const GfxColor WHITE;
    static const GfxColor RED;
    static const GfxColor GREEN;
    static const GfxColor BLUE;
    static const GfxColor YELLOW;
    static const GfxColor CYAN;
    static const GfxColor MAGENTA;
    static const GfxColor GRAY;
    static const GfxColor DARK_GRAY;
    static const GfxColor LIGHT_GRAY;

private:
    u8 m_r;
    u8 m_g;
    u8 m_b;
    u8 m_a;
};

constexpr const GfxColor GfxColor::TRANSPARENT = {0, 0, 0, 0};
constexpr const GfxColor GfxColor::BLACK = {0, 0, 0, 255};
constexpr const GfxColor GfxColor::WHITE = {255, 255, 255, 255};
constexpr const GfxColor GfxColor::RED = {255, 0, 0, 255};
constexpr const GfxColor GfxColor::GREEN = {0, 255, 0, 255};
constexpr const GfxColor GfxColor::BLUE = {0, 0, 255, 255};
constexpr const GfxColor GfxColor::YELLOW = {255, 255, 0, 255};
constexpr const GfxColor GfxColor::CYAN = {0, 255, 255, 255};
constexpr const GfxColor GfxColor::MAGENTA = {255, 0, 255, 255};
constexpr const GfxColor GfxColor::GRAY = {128, 128, 128, 255};
constexpr const GfxColor GfxColor::DARK_GRAY = {64, 64, 64, 255};
constexpr const GfxColor GfxColor::LIGHT_GRAY = {192, 192, 192, 255};
