#pragma once

#include <base/types.h>

enum class GfxPixelFormat
{
    UNKNOWN,
    GRAYSCALE_8,
    RGB_888,
    RGBA_8888,
    BGRA_8888,
};

struct __attribute__((packed)) GfxPixelGrayscale8 {
    u8 value;
};

struct __attribute__((packed)) GfxPixelRgb888 {
    u8 r;
    u8 g;
    u8 b;
};

struct __attribute__((packed)) GfxPixelRgba8888 {
    u8 r;
    u8 g;
    u8 b;
    u8 a;
};

struct __attribute__((packed)) GfxPixelBgra8888 {
    u8 b;
    u8 g;
    u8 r;
    u8 a;
};
