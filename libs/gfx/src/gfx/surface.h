#pragma once

#include <base/types.h>

#include <gfx/color.h>
#include <gfx/format.h>

class GfxSurface {
public:
    GfxSurface() = default;
    GfxSurface(u64 address, usize size, u32 width, u32 height, u32 pitch, GfxPixelFormat format)
        : m_address{address}, m_size{size}, m_width{width}, m_height{height}, m_pitch{pitch}, m_format{format} {}

    void clear();
    void fill(const GfxColor& color);
    void store(usize x, usize y, const GfxColor& color);
    void blit(usize x, usize y, const GfxSurface& other);

    GfxColor load(usize x, usize y) const;

    u64 address() const { return m_address; }
    u32 width() const { return m_width; }
    u32 height() const { return m_height; }
    u32 pitch() const { return m_pitch; }
    GfxPixelFormat format() const { return m_format; }

private:
    u64 m_address;
    usize m_size;
    u32 m_width;
    u32 m_height;
    u32 m_pitch;

    GfxPixelFormat m_format;
};
