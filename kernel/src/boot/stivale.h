#pragma once

#include <base/macros.h>
#include <base/types.h>

// Stivale specification can be found at: https://github.com/stivale/stivale/blob/master/STIVALE2.md
// The original implementation is available at: https://github.com/stivale/stivale/blob/master/stivale2.h
// This is a slightly changed version which is still compatible with the original implementation.

enum class StivaleStructTagId : u64
{
    PMRS = 0x5df266a64047b6bd,
    CMD_LINE = 0xe5e76a1b4597a781,
    MEMORY_MAP = 0x2187f79e8612de07,
    FRAMEBUFFER = 0x506461d2950408fa,
    TERMINAL = 0xc2b3f4c3233b0974,
    MODULES = 0x4b6fe466aade04ce,
    RSDP = 0x9e1786930a375e78,
    EPOCH = 0x566a7bed888e1407,
    KERNEL_FILE_V2 = 0x37c13018a02c6ea2,
    KERNEL_SLIDE = 0xee80847d01506c57,
    SMP = 0x34d1d96339647025,
    VMAP = 0xb0ed257db18cb58f,
};

enum class StivaleHeaderTagId : u64
{
    FRAMEBUFFER = 0x3ecc1bc43d0f7971,
    TERMINAL = 0xa85d499b1823be72,
    SMP = 0x1ab015085f3273df,
    PML5 = 0x932f477032007e8f,
    UNMAP_NULL = 0x92919432b16fe7e7,
};

enum class StivalePmrPermissions : u64
{
    EXECUTABLE = 1 << 0,
    WRITABLE = 1 << 1,
    READABLE = 1 << 2,
};

enum class StivaleMemoryMapType : u32
{
    USABLE = 1,
    RESERVED = 2,
    ACPI_RECLAIMABLE = 3,
    ACPI_NVS = 4,
    BAD_MEMORY = 5,
    BOOTLOADER_RECLAIMABLE = 0x1000,
    KERNEL_AND_MODULES = 0x1001,
    FRAMEBUFFER = 0x1002,
};

enum class StivaleTerminalStructFlags : u32
{
    NONE = 0,
    COLS_AND_ROWS_PROVIDED = 1 << 0,
    MAX_LENGTH_PROVIDED = 1 << 1,
    CALLBACK_SUPPORTED = 1 << 2,
    CONTEXT_CONTROL_AVAILABLE = 1 << 3,
};

enum class StivaleTerminalHeaderFlags : u64
{
    NONE = 0,
    HAS_CALLBACK = 1 << 0,
};

MAKE_ENUM_OPERATORS(StivalePmrPermissions);
MAKE_ENUM_OPERATORS(StivaleTerminalHeaderFlags);
MAKE_ENUM_OPERATORS(StivaleTerminalStructFlags);

using StivaleTermWriteFn = void (*)(const char *, usize);
using StivaleTermCallbackFn = void (*)(u64, u64, u64, u64);

struct StivaleStructTag {
    StivaleStructTagId id;
    StivaleStructTag *next;
};

struct StivaleHeaderTag {
    StivaleHeaderTagId id;
    StivaleHeaderTag *next;
};

struct StivalePmrEntry {
    u64 base;
    u64 size;
    StivalePmrPermissions permissions;
};

struct StivaleMemoryMapEntry {
    u64 base;
    u64 size;
    StivaleMemoryMapType type;
    u32 reserved;
};

struct StivaleModuleEntry {
    u64 start;
    u64 end;

    char name[128];
};

struct StivaleSmpEntry {
    u32 processor_id;
    u32 lapic_id;
    u64 *target_stack;
    u64 *goto_address;
    u64 *extra_argument;
};

struct StivaleStructPmrsTag {
    StivaleStructTag tag;
    u64 length;
    StivalePmrEntry entries[];
};

struct StivaleStructCmdLineTag {
    StivaleStructTag tag;
    const char *string;
};

struct StivaleStructMemoryMapTag {
    StivaleStructTag tag;
    u64 length;
    StivaleMemoryMapEntry entries[];
};

struct StivaleStructFramebufferTag {
    StivaleStructTag tag;
    u64 address;
    u16 width;
    u16 height;
    u16 pitch;
    u16 bit_depth;
    u8 memory_model;
    u8 red_mask_size;
    u8 red_mask_shift;
    u8 green_mask_size;
    u8 green_mask_shift;
    u8 blue_mask_size;
    u8 blue_mask_shift;
    u8 reserved;
};

struct StivaleStructTerminalTag {
    StivaleStructTag tag;
    StivaleTerminalStructFlags flags;
    u16 columns;
    u16 rows;
    StivaleTermWriteFn term_write;
    u64 max_length;
};

struct StivaleStructModulesTag {
    StivaleStructTag tag;
    u64 length;
    StivaleModuleEntry entries[];
};

struct StivaleStructRsdpTag {
    StivaleStructTag tag;
    u64 address;
};

struct StivaleStructEpochTag {
    StivaleStructTag tag;
    u64 epoch;
};

struct StivaleStructKernelFileV2Tag {
    StivaleStructTag tag;
    u64 address;
    u64 size;
};

struct StivaleStructKernelSlideTag {
    StivaleStructTag tag;
    u64 slide;
};

struct StivaleStructSmpTag {
    StivaleStructTag tag;
    u64 flags;
    u32 bsp_lapic;
    u32 reserved;
    u64 length;
    StivaleSmpEntry entries[];
};

struct StivaleStructVmapTag {
    StivaleStructTag tag;
    u64 vmap;
};

struct StivaleHeaderFramebufferTag {
    StivaleHeaderTag tag;
    u16 width;
    u16 height;
    u16 bit_depth;
    u16 reserved;
};

struct StivaleHeaderTerminalTag {
    StivaleHeaderTag tag;
    StivaleTerminalHeaderFlags flags;
    StivaleTermCallbackFn callback;
};

struct StivaleHeaderSmpTag {
    StivaleHeaderTag tag;
    u64 flags;
};

struct StivaleHeaderPml5Tag {
    StivaleHeaderTag tag;
};

struct StivaleHeaderUnmapNullTag {
    StivaleHeaderTag tag;
};

struct StivaleStruct {
    char loader_name[64];
    char loader_version[64];

    StivaleStructTag *tags;

    StivaleStructTag *find_tag(StivaleStructTagId id);
    StivaleModuleEntry *find_module(const char *name);
};

struct StivaleHeader {
    u64 entry_point;
    u64 stack_top;
    u64 loader_flags;
    StivaleHeaderTag *tags;
};
