#include <base/string.h>

#include <boot/stivale.h>

static u8 kernel_stack[8192];

static StivaleHeaderUnmapNullTag unmap_null = {
    .tag =
        {
            .id = StivaleHeaderTagId::UNMAP_NULL,
        },
};

static StivaleHeaderFramebufferTag framebuffer = {
    .tag =
        {
            .id = StivaleHeaderTagId::FRAMEBUFFER,
            .next = &unmap_null.tag,
        },
    .bit_depth = 32,
};

__attribute__((used, section(".stivale2hdr"))) static StivaleHeader header = {
    .entry_point = 0,  // Uses the default entry point
    .stack_top = (u64) kernel_stack + sizeof(kernel_stack),
    .loader_flags = 1 << 1 | 1 << 2,
    .tags = &framebuffer.tag,
};

StivaleStructTag *StivaleStruct::find_tag(StivaleStructTagId id) {
    for (auto current = tags; current; current = current->next) {
        if (current->id == id)
            return current;
    }

    return nullptr;
}

StivaleModuleEntry *StivaleStruct::find_module(const char *name) {
    auto modules = (StivaleStructModulesTag *) find_tag(StivaleStructTagId::MODULES);

    if (!modules)
        return nullptr;

    for (auto i = 0u; i < modules->length; i++) {
        auto module = &modules->entries[i];

        if (string_compare(module->name, name) == 0)
            return module;
    }

    return nullptr;
}
