#include <boot/stivale.h>

#include <cpu/gdt.h>
#include <cpu/idt.h>

#include <memory/heap.h>
#include <memory/paging.h>
#include <memory/pmm.h>

#include <utils/backtrace.h>
#include <utils/console.h>
#include <utils/print.h>

extern "C" void kernel_main(StivaleStruct *boot_info) {
    asm volatile("cli");

    auto framebuffer = (StivaleStructFramebufferTag *) boot_info->find_tag(StivaleStructTagId::FRAMEBUFFER);

    utils::console::initialize(framebuffer);

    auto terminal = (StivaleStructTerminalTag *) boot_info->find_tag(StivaleStructTagId::TERMINAL);
    auto kernel_file = (StivaleStructKernelFileV2Tag *) boot_info->find_tag(StivaleStructTagId::KERNEL_FILE_V2);

    utils::load_symbols(kernel_file);

    utils::print("hello, kernel world!\n");
    utils::print_format("loaded using %s %s\n", boot_info->loader_name, boot_info->loader_version);

    gdt::initialize();
    idt::initialize();

    auto pmrs = (StivaleStructPmrsTag *) boot_info->find_tag(StivaleStructTagId::PMRS);
    auto memory_map = (StivaleStructMemoryMapTag *) boot_info->find_tag(StivaleStructTagId::MEMORY_MAP);

    // TODO: Assert to make sure the tags exist

    memory::pmm::initialize(memory_map);
    memory::paging::initialize(pmrs);
    memory::heap::initialize(0xffffffff20000000, 4000);  // 16MiB

    utils::print_format("heap statistics: total=%dKiB, used=%dKiB, free=%dKiB\n", memory::heap::get_total_memory() / 1024,
                        memory::heap::get_used_memory() / 1024, memory::heap::get_free_memory() / 1024);

    utils::print_format("physical memory statistics: total=%dKiB, used=%dKiB, free=%dKiB\n", memory::pmm::get_total_memory() / 1024,
                        memory::pmm::get_used_memory() / 1024, memory::pmm::get_free_memory() / 1024);

    utils::print("we successfully booted up!\n");

    while (true) {
        asm volatile("hlt");
    }
}
