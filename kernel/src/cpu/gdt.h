#pragma once

#include <base/macros.h>
#include <base/types.h>

enum class GdtSelector : u16
{
    NULL = 0x00,
    KERNEL_CODE = 0x08,
    KERNEL_DATA = 0x10,
    USER_CODE = 0x18,
    USER_DATA = 0x20,
    TSS = 0x28,
};

enum class GdtAccess : u8
{
    NONE = 0,
    WRITABLE = 1 << 1,
    CONFORMING = 1 << 2,
    CODE = 1 << 3,
    DATA = 1 << 4,
    RING_0 = 0 << 5,
    RING_1 = 1 << 5,
    RING_2 = 2 << 5,
    RING_3 = 3 << 5,
    PRESENT = 1 << 7,
};

enum class GdtFlags : u8
{
    NONE = 0,
    LONG = 1 << 5,
    SIZE = 1 << 6,
    GRANULARITY = 1 << 7,
};

MAKE_ENUM_OPERATORS(GdtAccess)
MAKE_ENUM_OPERATORS(GdtFlags)

struct __attribute__((packed)) Gdtr {
    u16 limit;
    u64 base;
};

struct __attribute__((packed)) GdtDescriptor {
    u16 limit_low;
    u16 base_low;
    u8 base_middle;
    u8 access;
    u8 flags;  // also limit_high
    u8 base_high;

    constexpr GdtDescriptor() = default;
    constexpr GdtDescriptor(GdtAccess access, GdtFlags flags)
        : limit_low{0}, base_low{0}, base_middle{0}, access{(u8) (access | GdtAccess::PRESENT)}, flags{(u8) flags}, base_high{0} {}
};

struct __attribute__((packed)) GdtXdescriptor {
    GdtDescriptor low;

    u32 base_xhigh;
    u32 reserved;

    constexpr GdtXdescriptor() = default;
    constexpr GdtXdescriptor(u64 base, u64 limit, GdtAccess access, GdtFlags flags)
        : low{access, (GdtFlags) ((u8) flags | ((limit >> 16) & 0x0f))}, base_xhigh{(u32) (base >> 32) & 0xffffffff}, reserved{0} {}
};

namespace gdt {

void initialize();

}  // namespace gdt
