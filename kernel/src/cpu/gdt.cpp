#include <base/memory.h>
#include <core/array.h>

#include <cpu/gdt.h>
#include <utils/print.h>

static constexpr auto CODE_SEGMENT = GdtAccess::DATA | GdtAccess::CODE | GdtAccess::WRITABLE;
static constexpr auto DATA_SEGMENT = GdtAccess::DATA | GdtAccess::WRITABLE;

static constexpr Array m_gdt_array{
    GdtDescriptor(),                                                  // Null descriptor
    GdtDescriptor(CODE_SEGMENT, GdtFlags::LONG),                      // Kernel code segment
    GdtDescriptor(DATA_SEGMENT, GdtFlags::LONG),                      // Kernel data segment
    GdtDescriptor(CODE_SEGMENT | GdtAccess::RING_3, GdtFlags::LONG),  // User code segment
    GdtDescriptor(DATA_SEGMENT | GdtAccess::RING_3, GdtFlags::LONG),  // User data segment
};

void gdt::initialize() {
    Gdtr gdtr;

    gdtr.base = (u64) m_gdt_array.data();
    gdtr.limit = m_gdt_array.size() * sizeof(GdtDescriptor) - 1;

    asm volatile(
        "lgdt %0\n"
        "movw %1, %%ds\n"
        "movw %1, %%ss\n"
        :
        : "m"(gdtr), "r"((u16) GdtSelector::KERNEL_DATA));

    u64 lretq_address;

    asm volatile(
        "pushq %0\n"
        "leaq  1f(%%rip), %1\n"
        "pushq %1\n"
        "lretq\n"
        "1:\n"
        : "=r"(lretq_address)
        : "r"((u64) GdtSelector::KERNEL_CODE));

    utils::print_format("successfully loaded the GDT (base=%x, limit=%x)\n", gdtr.base, gdtr.limit);
}
