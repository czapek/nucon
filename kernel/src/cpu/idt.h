#pragma once

#include <base/macros.h>
#include <base/types.h>

#include <cpu/gdt.h>

enum class IdtInterruptType : u8
{
    INTERRUPT_GATE_16 = 0b0110,
    INTERRUPT_GATE_32 = 0b1110,
    TRAP_GATE_16 = 0b0111,
    TRAP_GATE_32 = 0b1111,
    TASK_GATE = 0b0101,
};

enum class IdtDescriptorFlags : u8
{
    NONE = 0,
    PRESENT = 1 << 7,
    RING_0 = 0 << 5,
    RING_1 = 1 << 5,
    RING_2 = 2 << 5,
    RING_3 = 3 << 5,
};

MAKE_ENUM_OPERATORS(IdtInterruptType);
MAKE_ENUM_OPERATORS(IdtDescriptorFlags);

struct IsrStackFrame {
    u64 rip;
    u64 code_segment;
    u64 cpu_flags;
    u64 rsp;
    u64 stack_segment;
};

struct __attribute__((packed)) Idtr {
    u16 limit;
    u64 base;
};

struct __attribute__((packed)) IdtDescriptor {
    u16 base_low;
    u16 selector;
    u8 ist;
    u8 type_attr;
    u16 base_mid;
    u32 base_high;
    u32 reserved;

    constexpr IdtDescriptor() = default;
    constexpr IdtDescriptor(u64 base, GdtSelector selector, u8 ist, IdtInterruptType type, IdtDescriptorFlags flags)
        : base_low{(u16) (base & 0xffff)},
          selector{(u16) selector},
          ist{(u8) (ist & 7)},
          type_attr{(u8) ((u8) type | (u8) flags)},
          base_mid{(u16) ((base >> 16) & 0xffff)},
          base_high{(u32) (base >> 32)},
          reserved{0} {}
};

namespace idt {

void initialize();

}  // namespace idt
