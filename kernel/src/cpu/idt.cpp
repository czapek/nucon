#include <core/array.h>

#include <cpu/idt.h>
#include <utils/print.h>

void __attribute__((interrupt)) unhandled_isr(IsrStackFrame *stack_frame);
void __attribute__((interrupt)) invalid_opcode_isr(IsrStackFrame *stack_frame);
void __attribute__((interrupt)) double_fault_isr(IsrStackFrame *stack_frame);
void __attribute__((interrupt)) gp_fault_isr(IsrStackFrame *stack_frame, u64 error_code);
void __attribute__((interrupt)) page_fault_isr(IsrStackFrame *stack_frame, u64 error_code);

static Array<IdtDescriptor, 256> m_idt_array;

void idt::initialize() {
    for (auto i = 0; i < 256; i++) {
        m_idt_array[i] = {(u64) unhandled_isr, GdtSelector::KERNEL_CODE, 0, IdtInterruptType::INTERRUPT_GATE_32,
                          IdtDescriptorFlags::PRESENT};
    }

    m_idt_array[6] = {(u64) invalid_opcode_isr, GdtSelector::KERNEL_CODE, 0, IdtInterruptType::INTERRUPT_GATE_32,
                      IdtDescriptorFlags::PRESENT};
    m_idt_array[8] = {(u64) double_fault_isr, GdtSelector::KERNEL_CODE, 0, IdtInterruptType::INTERRUPT_GATE_32,
                      IdtDescriptorFlags::PRESENT};
    m_idt_array[13] = {(u64) gp_fault_isr, GdtSelector::KERNEL_CODE, 0, IdtInterruptType::INTERRUPT_GATE_32, IdtDescriptorFlags::PRESENT};
    m_idt_array[14] = {(u64) page_fault_isr, GdtSelector::KERNEL_CODE, 0, IdtInterruptType::INTERRUPT_GATE_32, IdtDescriptorFlags::PRESENT};

    Idtr idtr;

    idtr.base = (u64) m_idt_array.data();
    idtr.limit = m_idt_array.size() * sizeof(IdtDescriptor) - 1;

    asm volatile(
        "lidt %0\n"
        "sti\n"
        :
        : "m"(idtr));

    utils::print_format("successfully loaded the IDT (base=%x, limit=%x)\n", idtr.base, idtr.limit);
}
