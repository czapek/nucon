#pragma once

#include <base/types.h>

#include <gfx/color.h>

#include <boot/stivale.h>

namespace utils::console {

void initialize(StivaleStructFramebufferTag *fb);
void set_background(const GfxColor &color);
void set_foreground(const GfxColor &color);
void put_char(char ch);

}  // namespace utils::console
