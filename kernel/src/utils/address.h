#pragma once

#include <base/types.h>

constexpr inline u64 align_down(u64 address, u64 alignment) {
    return address & ~(alignment - 1);
}

constexpr inline u64 align_up(u64 address, u64 alignment) {
    return align_down(address + alignment - 1, alignment);
}
