#include <base/memory.h>

#include <gfx/color.h>
#include <gfx/surface.h>

#include <utils/address.h>
#include <utils/console.h>
#include <utils/font.h>

static GfxSurface m_framebuffer;
static GfxColor m_background;
static GfxColor m_foreground;

static usize m_columns;
static usize m_rows;

static usize m_cursor_x;
static usize m_cursor_y;

static void scroll_screen() {
    // Scroll all lines up by one line
    for (auto y = 1; y < m_rows; y++) {
        const auto source_y = y * 16;
        const auto dest_y = source_y - 16;

        for (auto i = 0; i < 16; i++) {
            auto source_line = m_framebuffer.address() + (source_y + i) * m_framebuffer.pitch();
            auto dest_line = m_framebuffer.address() + (dest_y + i) * m_framebuffer.pitch();

            memory_copy((u8 *) dest_line, (u8 *) source_line, m_framebuffer.pitch());
        }
    }

    // Clear the last line
    auto last_line = m_framebuffer.address() + (m_rows - 1) * 16 * m_framebuffer.pitch();

    for (auto i = 0; i < 16; i++) {
        auto line = last_line + i * m_framebuffer.pitch();

        for (auto x = 0; x < m_framebuffer.width(); x++) {
            m_framebuffer.store(x, line, m_background);
        }
    }

    // Update cursor position
    m_cursor_x = 0;
    m_cursor_y = m_rows - 1;
}

void utils::console::initialize(StivaleStructFramebufferTag *fb) {
    auto fb_size = fb->width * (fb->bit_depth / 8) + fb->height * fb->pitch;
    auto surface = GfxSurface(fb->address, fb_size, fb->width, fb->height, fb->pitch, GfxPixelFormat::BGRA_8888);

    m_framebuffer = surface;
    m_background = GfxColor::BLACK;
    m_foreground = GfxColor::LIGHT_GRAY;

    m_framebuffer.fill(m_background);

    m_columns = fb->width / 8;
    m_rows = fb->height / 16;

    m_cursor_x = 0;
    m_cursor_y = 0;
}

void utils::console::set_background(const GfxColor &color) {
    m_background = color;
}

void utils::console::set_foreground(const GfxColor &color) {
    m_foreground = color;
}

void utils::console::put_char(char ch) {
    if (ch == '\n') {
        m_cursor_x = 0;
        m_cursor_y++;
    } else if (ch == '\t') {
        m_cursor_x = align_up(m_cursor_x, 4);
    } else {
        if (m_cursor_x >= m_columns)
            put_char('\n');

        if (m_cursor_y >= m_rows)
            scroll_screen();

        for (auto y = 0; y < 16; y++) {
            const auto pixel_y = m_cursor_y * 16 + y;
            const auto &font_row = m_font_data[(usize) ch][y];

            for (auto x = 0; x < 8; x++) {
                if (font_row & (1 << (7 - x)))
                    m_framebuffer.store(m_cursor_x * 8 + x, pixel_y, m_foreground);
                else
                    m_framebuffer.store(m_cursor_x * 8 + x, pixel_y, m_background);
            }
        }

        m_cursor_x++;
    }
}
