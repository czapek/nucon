#include <base/memory.h>
#include <core/array.h>

#include <utils/console.h>
#include <utils/print.h>

static constexpr Array m_integer_digits{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

static char get_integer_prefix(IntegerBase base) {
    switch (base) {
        case IntegerBase::BINARY:
            return 'b';
        case IntegerBase::OCTAL:
            return 'o';
        case IntegerBase::DECIMAL:
            return '\0';
        case IntegerBase::HEXADECIMAL:
            return 'x';
    };

    return '\0';
}

void utils::print(char ch) {
    console::put_char(ch);
}

void utils::print(StringView string) {
    for (auto ch : string) {
        print(ch);
    }
}

void utils::print(u64 value, IntegerBase base) {
    char buffer[64 + 2];

    auto length = 0u;
    auto prefix = get_integer_prefix(base);

    do {
        buffer[length++] = m_integer_digits[value % (u64) base];
        value /= (u64) base;
    } while (value != 0);

    if (prefix != '\0') {
        buffer[length++] = prefix;
        buffer[length++] = '0';
    }

    while (length > 0) {
        print(buffer[--length]);
    }
}

void utils::print_format(StringView format, va_list arguments) {
    for (auto i = 0u; i < format.size(); i++) {
        const auto ch = format[i];

        if (ch == '%') {
            // TODO: Add support for l and ll format pre-/postfixes
            switch (format[++i]) {
                case '\0':
                    break;
                case '%':
                    print('%');
                    break;
                case 'c':
                    print((char) va_arg(arguments, int));
                    break;
                case 's':
                    print(va_arg(arguments, const char *));
                    break;
                case 'b':
                    print((u64) va_arg(arguments, u64), IntegerBase::BINARY);
                    break;
                case 'o':
                    print((u64) va_arg(arguments, u64), IntegerBase::OCTAL);
                    break;
                case 'd':
                    print((u64) va_arg(arguments, u64), IntegerBase::DECIMAL);
                    break;
                case 'x':
                    print((u64) va_arg(arguments, u64), IntegerBase::HEXADECIMAL);
                    break;
                default:
                    print('%');
                    print(format[i]);
                    break;
            }
        } else {
            print(ch);
        }
    }
}

void utils::print_format(StringView format, ...) {
    va_list arguments;

    va_start(arguments, format);

    print_format(format, arguments);

    va_end(arguments);
}
