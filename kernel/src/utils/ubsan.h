#pragma once

#include <base/types.h>
#include <core/string_view.h>

class SourceLocation {
public:
    bool is_valid() const { return m_filename; }

    StringView get_filename() const { return m_filename; }
    u32 get_line() const { return m_line; }
    u32 get_column() const { return m_column; }

private:
    const char *m_filename;
    u32 m_line;
    u32 m_column;
};

class TypeDescriptor {
public:
    enum class TypeKind : u16
    {
        INTEGER = 0x0000,
        FLOAT = 0x0001,
        UNKNOWN = 0xffff
    };

    StringView get_name() const { return m_type_name; }
    TypeKind get_kind() const { return (TypeKind) m_type_kind; }

    bool is_integer() const { return get_kind() == TypeKind::INTEGER; }
    bool is_signed_integer() const { return is_integer() && (m_type_info & 1); }
    bool is_unsigned_integer() const { return is_integer() && !(m_type_info & 1); }
    bool is_float() const { return get_kind() == TypeKind::FLOAT; }

    u64 get_integer_bit_width() const { return 1 << (m_type_info >> 1); }
    u64 get_float_bit_width() const { return m_type_info; }

private:
    u16 m_type_kind;
    u16 m_type_info;
    char m_type_name[];
};

class Value {
    bool is_inline_int() const;
    bool is_inline_float() const;

public:
    Value(const TypeDescriptor &type, u64 handle) : m_type{type}, m_handle{handle} {}

    i64 get_i64_value() const;
    u64 get_u64_value() const;
    u64 get_positive_int_value() const;

    bool is_minus_one() const { return m_type.is_signed_integer() && get_i64_value() == -1; }
    bool is_negative() const { return m_type.is_signed_integer() && get_i64_value() < 0; }

private:
    const TypeDescriptor &m_type;
    u64 m_handle;
};

struct TypeMismatchData {
    SourceLocation location;
    const TypeDescriptor &type;
    u8 log_alignment;
    u8 type_check_kind;
};

struct PointerOverflowData {
    SourceLocation location;
};

struct OverflowData {
    SourceLocation location;
    const TypeDescriptor &type;
};

struct OutOfBoundsData {
    SourceLocation location;
    const TypeDescriptor &array_type;
    const TypeDescriptor &index_type;
};

struct ShiftOutOfBoundsData {
    SourceLocation location;
    const TypeDescriptor &lhs_type;
    const TypeDescriptor &rhs_type;
};

struct UnreachableData {
    SourceLocation location;
};

struct InvalidValueData {
    SourceLocation location;
    const TypeDescriptor &type;
};
