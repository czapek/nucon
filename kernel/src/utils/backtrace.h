#pragma once

#include <boot/stivale.h>

struct StackFrame {
    StackFrame* rbp;
    u64 rip;
};

namespace utils {

void load_symbols(StivaleStructKernelFileV2Tag* kernel_file);
void print_backtrace();

}  // namespace utils
