#include <elf/header.h>
#include <elf/section.h>
#include <elf/symbol_table.h>

#include <utils/backtrace.h>
#include <utils/print.h>

static ElfHeader64* m_elf_header;
static ElfSectionHeader64* m_symbol_table;
static ElfSectionHeader64* m_string_table;

struct SymbolInfo {
    u64 address;
    StringView name;
};

static SymbolInfo find_symbol_for(u64 address) {
    const auto symbol_table_start = (u64) m_elf_header + m_symbol_table->offset;
    const auto string_table_start = (u64) m_elf_header + m_string_table->offset;

    auto symbol_array = (ElfSymbolEntry64*) symbol_table_start;
    auto symbol_count = m_symbol_table->size / sizeof(ElfSymbolEntry64);

    for (auto i = 0u; i < symbol_count; i++) {
        const auto& symbol = symbol_array[i];

        if (address < symbol.value || address > symbol.value + symbol.size)
            continue;

        if (symbol.name < m_string_table->size)
            return {symbol.value, (const char*) string_table_start + symbol.name};
    }

    return {0};
}

void utils::load_symbols(StivaleStructKernelFileV2Tag* kernel_file) {
    m_elf_header = (ElfHeader64*) kernel_file->address;

    // Make sure the file is a valid ELF file
    // TODO: Implement some sort of panic function
    if (string_compare((const char*) m_elf_header->identifier, "\177ELF", 4) != 0) {
        utils::print("given kernel file is not a valid ELF file\n");

        while (true) {
            asm volatile("hlt");
        }
    }

    // Iterate ELF sections
    for (auto i = 0u; i < m_elf_header->section_header_count; i++) {
        auto section =
            (ElfSectionHeader64*) (kernel_file->address + m_elf_header->section_header_offset + m_elf_header->section_header_size * i);
        auto type = (ElfSectionType) section->type;

        if (type == ElfSectionType::SYM_TAB && !m_symbol_table) {
            m_symbol_table = section;
        } else if (type == ElfSectionType::STR_TAB && m_symbol_table && i == m_symbol_table->link) {
            m_string_table = section;
        }
    }

    // Make sure that we have both symbol and string tables
    if (!m_symbol_table)
        utils::print("no symbol table found\n");

    if (!m_string_table)
        utils::print("no string table found\n");
}

void utils::print_backtrace() {
    StackFrame* stack_frame;

    asm volatile("movq %%rbp, %0" : "=r"(stack_frame));

    utils::print_format("= backtrace:\n");

    while (stack_frame) {
        const auto symbol = find_symbol_for(stack_frame->rip);

        if (symbol.address)
            utils::print_format("== %s+%x (%x)\n", symbol.name.data(), stack_frame->rip - symbol.address, stack_frame->rip);
        else
            utils::print_format("== %x\n", stack_frame->rip);

        stack_frame = stack_frame->rbp;
    }
}
