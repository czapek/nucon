#include <base/types.h>
#include <core/array.h>
#include <core/string_view.h>

#include <utils/print.h>
#include <utils/ubsan.h>

bool Value::is_inline_int() const {
    const auto inline_bits = sizeof(u64) * 8;
    const auto type_bits = m_type.get_integer_bit_width();

    return type_bits <= inline_bits;
}

bool Value::is_inline_float() const {
    const auto inline_bits = sizeof(u64) * 8;
    const auto type_bits = m_type.get_float_bit_width();

    return type_bits <= inline_bits;
}

i64 Value::get_i64_value() const {
    if (is_inline_int()) {
        const auto extra_bits = sizeof(i64) * 8 - m_type.get_integer_bit_width();

        return ((i64) m_handle) << extra_bits >> extra_bits;
    } else if (m_type.get_integer_bit_width() == 64) {
        return *(i64 *) m_handle;
    }

    return 0;
}

u64 Value::get_u64_value() const {
    if (is_inline_int())
        return m_handle;
    else if (m_type.get_integer_bit_width() == 64)
        return *(u64 *) m_handle;

    return 0;
}

u64 Value::get_positive_int_value() const {
    if (m_type.is_unsigned_integer())
        return get_u64_value();

    return get_i64_value();
}

static void ubsan_print_location(const SourceLocation &location, bool deadly) {
    utils::print_format(" on file=%s, line=%d, column=%d\n", location.get_filename().data(), location.get_line(), location.get_column());

    while (deadly) {
        asm volatile("hlt");
    }
}

extern "C" void __ubsan_handle_type_mismatch_v1(TypeMismatchData &data, u64 pointer) {
    static constexpr Array type_check_kinds{"load of",
                                            "store to",
                                            "reference binding to",
                                            "member access within",
                                            "member call on",
                                            "constructor call on",
                                            "downcast of",
                                            "downcast of",
                                            "upcast of",
                                            "cast to virtual base of",
                                            "cast to interface of",
                                            "non-null binding to",
                                            "dynamic operation on"};

    if (pointer) {
        const auto alignment = 1 << data.log_alignment;
        const auto message = pointer & (alignment - 1) ? "ubsan: %s a misaligned address %x for type %s, which requires alignment of %d"
                                                       : "ubsan: %s an address %x with insufficient space for an object of type %s";

        utils::print_format(message, type_check_kinds[data.type_check_kind], pointer, data.type.get_name().data(), alignment);
    } else {
        utils::print_format("ubsan: %s a null pointer of type %s", type_check_kinds[data.type_check_kind], data.type.get_name().data());
    }

    ubsan_print_location(data.location, true);
}

extern "C" void __ubsan_handle_pointer_overflow(PointerOverflowData &data, u64 base, u64 result) {
    if (base == 0 && result == 0) {
        utils::print("ubsan: applying a zero offset to a null pointer");
    } else if (base == 0 && result != 0) {
        utils::print_format("ubsan: applying a non-zero offset %x to a null pointer", result);
    } else if (base != 0 && result == 0) {
        utils::print_format("ubsan: applying a non-zero offset to a non-null pointer %x resulted in a null pointer", base);
    } else {
        const auto signed_base = (isize) base;
        const auto signed_result = (isize) result;

        if ((signed_base >= 0) == (signed_result >= 0)) {
            if (base > result)
                utils::print_format("ubsan: applying a non-zero offset to a non-null pointer %x overflowed to %x", base, result);
            else
                utils::print_format("ubsan: applying a non-zero offset to a non-null pointer %x underflowed to %x", base, result);
        } else {
            utils::print_format("ubsan: pointer index expression with base %x overflowed to %x", base, result);
        }
    }

    ubsan_print_location(data.location, true);
}

extern "C" void __ubsan_handle_missing_return(UnreachableData &data) {
    utils::print("ubsan: execution reached the end of a value-returning function without returning a value");
    ubsan_print_location(data.location, true);
}

#define MAKE_OVERFLOW_HANDLER(name, op)                                                                                 \
    extern "C" void name(OverflowData &data, u64 lhs, u64 rhs) {                                                        \
        const auto message = data.type.is_signed_integer()                                                              \
                                 ? "ubsan: signed integer overflow, %d " op " %d cannot be represented with type %s"    \
                                 : "ubsan: unsigned integer overflow, %d " op " %d cannot be represented with type %s"; \
        utils::print_format(message, lhs, rhs, data.type.get_name().data());                                            \
        ubsan_print_location(data.location, true);                                                                      \
    }

MAKE_OVERFLOW_HANDLER(__ubsan_handle_add_overflow, "+")
MAKE_OVERFLOW_HANDLER(__ubsan_handle_sub_overflow, "-")
MAKE_OVERFLOW_HANDLER(__ubsan_handle_mul_overflow, "*")

extern "C" void __ubsan_handle_divrem_overflow(OverflowData &data, u64 lhs, u64 rhs) {
    const Value lhs_value{data.type, lhs};
    const Value rhs_value{data.type, rhs};

    if (rhs_value.is_minus_one())
        utils::print_format("ubsan: division of %d by -1 cannot be represented with type %s", lhs_value.get_i64_value(),
                            data.type.get_name().data());
    else if (data.type.is_integer())
        utils::print("ubsan: integer division by zero");
    else
        utils::print("ubsan: float division by zero");

    ubsan_print_location(data.location, true);
}

extern "C" void __ubsan_handle_out_of_bounds(OutOfBoundsData &data, u64 index) {
    const Value index_value{data.index_type, index};

    utils::print_format("ubsan: index %d is out of bounds for type %s", index_value.get_u64_value(), data.array_type.get_name().data());
    ubsan_print_location(data.location, true);
}

extern "C" void __ubsan_handle_shift_out_of_bounds(ShiftOutOfBoundsData &data, u64 lhs, u64 rhs) {
    const Value lhs_value{data.lhs_type, lhs};
    const Value rhs_value{data.rhs_type, rhs};

    if (rhs_value.is_negative()) {
        utils::print_format("ubsan: shift exponent %d is negative", rhs_value.get_i64_value());
    } else if (rhs_value.get_positive_int_value() >= data.lhs_type.get_integer_bit_width()) {
        utils::print_format("ubsan: shift exponent %d is too large for %d-bit type %s", rhs_value.get_i64_value(),
                            data.lhs_type.get_integer_bit_width(), data.lhs_type.get_name().data());
    } else if (lhs_value.is_negative()) {
        utils::print_format("ubsan: left shift of a negative value %d", lhs_value.get_i64_value());
    } else {
        utils::print_format("ubsan: left shift of %d by %d bits cannot be represented in type %s", lhs_value.get_i64_value(),
                            rhs_value.get_u64_value(), data.lhs_type.get_name().data());
    }

    ubsan_print_location(data.location, true);
}

extern "C" void __ubsan_handle_builtin_unreachable(UnreachableData &data) {
    utils::print("ubsan: execution reached an unreachable program point");
    ubsan_print_location(data.location, true);
}

extern "C" void __ubsan_handle_load_invalid_value(InvalidValueData &data, u64 value) {
    const Value load_value{data.type, value};

    utils::print_format("ubsan: load of and invalid value %x for type %s", load_value.get_u64_value(), data.type.get_name().data());
    ubsan_print_location(data.location, true);
}
