#pragma once

#include <stdarg.h>

#include <base/types.h>
#include <core/string_view.h>

#include <boot/stivale.h>

enum class IntegerBase : u64
{
    BINARY = 2,
    OCTAL = 8,
    DECIMAL = 10,
    HEXADECIMAL = 16,
};

namespace utils {

void set_term_write(StivaleTermWriteFn term_write);

void print(char ch);
void print(StringView string);
void print(u64 value, IntegerBase base = IntegerBase::DECIMAL);

// TODO: Implement this using templates
void print_format(StringView format, va_list arguments);
void print_format(StringView format, ...);

}  // namespace utils
