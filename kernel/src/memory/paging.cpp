#include <base/memory.h>

#include <memory/paging.h>
#include <memory/pmm.h>

#include <utils/address.h>
#include <utils/print.h>

static PageTable *m_kernel_pml4;

static PageTable *get_page_table(PageTable *parent, usize index, bool alloc_if_missing = true) {
    auto &entry = parent->get(index);

    if ((u64) entry.flags() & (u64) PageTableEntryFlags::PRESENT)
        return (PageTable *) entry.address();
    else if (!alloc_if_missing)
        return nullptr;

    auto table = (PageTable *) memory::pmm::allocate();

    // utils::print_format("allocated a page table at %x\n", (u64) table);

    entry.set_address((u64) table);
    entry.set_flags(PageTableEntryFlags::PRESENT | PageTableEntryFlags::WRITE);

    return table;
}

void memory::paging::initialize(StivaleStructPmrsTag *pmrs) {
    const auto kernel_pml4 = pmm::allocate();

    m_kernel_pml4 = (PageTable *) (kernel_pml4 + 0xffff800000000000);

    utils::print("identity mapping first 4GiB - null page\n");

    map_pages(0x1000, 0x1000, 0xfffff, PageTableEntryFlags::WRITE | PageTableEntryFlags::PRESENT);

    utils::print("mapping first 4GiB at 0xffff800000000000\n");

    map_pages(0xffff800000000000, 0, 0x100000, PageTableEntryFlags::WRITE | PageTableEntryFlags::PRESENT);

    utils::print("= mapping kernel memory ranges\n");

    for (auto i = 0u; i < pmrs->length; i++) {
        const auto &pmr = pmrs->entries[i];
        const auto base = align_down(pmr.base, 4096);
        const auto size = align_up(pmr.size, 4096);

        utils::print_format("== base=%x, size=%x, permissions=%x\n", base, size, (u64) pmr.permissions);

        map_pages(base, base - 0xffffffff80000000, size / 4096, PageTableEntryFlags::WRITE | PageTableEntryFlags::PRESENT);
    }

    utils::print("switching to new kernel page table\n");

    map_page(0xffffffff80000000, 0, PageTableEntryFlags::NONE);

    asm volatile("mov %0, %%cr3" : : "r"(kernel_pml4));

    utils::print_format("successfully switched the kernel page table to %x\n", kernel_pml4);
}

void memory::paging::map_page(u64 virtual_address, u64 physical_address, PageTableEntryFlags flags) {
    const auto pml4_index = (virtual_address >> 39) & 0x1ff;
    const auto pml3_index = (virtual_address >> 30) & 0x1ff;
    const auto pml2_index = (virtual_address >> 21) & 0x1ff;
    const auto pml1_index = (virtual_address >> 12) & 0x1ff;

    // utils::print_format("mapping page: virtual=%x, physical=%x, flags=%x\n", virtual_address, physical_address, (u64) flags);
    // utils::print_format("page table indices: pml4=%x, pml3=%x, pml2=%x, pml1=%x\n", pml4_index, pml3_index, pml2_index, pml1_index);

    auto pml3 = get_page_table(m_kernel_pml4, pml4_index);
    auto pml2 = get_page_table(pml3, pml3_index);
    auto pml1 = get_page_table(pml2, pml2_index);
    auto &entry = pml1->get(pml1_index);

    entry.set_address(physical_address);
    entry.set_flags(flags);
}

void memory::paging::map_pages(u64 virtual_start, u64 physical_start, u64 count, PageTableEntryFlags flags) {
    for (auto i = 0u; i < count; i++) {
        map_page(virtual_start + i * 4096, physical_start + i * 4096, flags);
    }
}

void memory::paging::unmap_page(u64 virtual_address) {
    const auto pml4_index = (virtual_address >> 39) & 0x1ff;
    const auto pml3_index = (virtual_address >> 30) & 0x1ff;
    const auto pml2_index = (virtual_address >> 21) & 0x1ff;
    const auto pml1_index = (virtual_address >> 12) & 0x1ff;

    if (auto pml3 = get_page_table(m_kernel_pml4, pml4_index, false)) {
        if (auto pml2 = get_page_table(pml3, pml2_index, false)) {
            if (auto pml1 = get_page_table(pml2, pml1_index, false)) {
                auto &entry = pml1->operator[](pml1_index);

                entry.set_address(0);
                entry.set_flags(PageTableEntryFlags::NONE);
            }
        }
    }
}

void memory::paging::unmap_pages(u64 virtual_start, u64 count) {
    for (auto i = 0u; i < count; i++) {
        unmap_page(virtual_start + i * 4096);
    }
}
