#include <memory/bitmap.h>

void Bitmap::initialize(u8 *bitmap, usize size) {
    m_bitmap = bitmap;
    m_size = size;
}

void Bitmap::clear(bool value) {
    for (auto i = 0u; i < m_size; i++) {
        m_bitmap[i] = value ? -1 : 0;
    }
}

bool Bitmap::get(usize index) {
    const auto byte = index / 8;
    const auto bit = index % 8;

    return m_bitmap[byte] & (1 << bit);
}

void Bitmap::set(usize index, bool value) {
    const auto byte = index / 8;
    const auto bit = index % 8;

    if (value) {
        m_bitmap[byte] |= (1 << bit);
    } else {
        m_bitmap[byte] &= ~(1 << bit);
    }
}

void Bitmap::set_range(usize index, usize count, bool value) {
    for (auto i = 0u; i < count; i++) {
        set(index + i, value);
    }
}

usize Bitmap::find_range(usize start, usize size, bool value) {
    for (auto i = start; i < m_size * 8 - size; i++) {
        if (get(i) != value)
            continue;

        auto found = true;

        for (auto j = 0; found && j < size; j++) {
            if (get(i + j) != value)
                found = false;
        }

        if (found)
            return i;
    }

    return -1;
}
