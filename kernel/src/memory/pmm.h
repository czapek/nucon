#pragma once

#include <base/types.h>

#include <boot/stivale.h>

namespace memory::pmm {

void initialize(StivaleStructMemoryMapTag *memory_map);

u64 allocate();
u64 allocate(usize count);

void free(u64 address);
void free(u64 address, usize count);

u64 get_total_memory();
u64 get_free_memory();
u64 get_used_memory();

}  // namespace memory::pmm
