#pragma once

#include <base/types.h>

class Bitmap {
public:
    void initialize(u8 *bitmap, usize size);
    void clear(bool value = false);

    bool get(usize index);

    void set(usize index, bool value);
    void set_range(usize index, usize count, bool value);

    usize find_range(usize start, usize size, bool value);

private:
    u8 *m_bitmap;
    usize m_size;
};
