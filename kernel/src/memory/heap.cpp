#include <memory/bitmap.h>
#include <memory/heap.h>
#include <memory/paging.h>
#include <memory/pmm.h>

#include <utils/address.h>
#include <utils/print.h>

static u64 m_heap_base;
static Bitmap m_heap_bitmap;
static usize m_preferred_allocation;
static u64 m_total_memory;
static u64 m_used_memory;

void memory::heap::initialize(u64 virtual_base, u64 page_count) {
    m_heap_base = virtual_base;

    for (auto i = 0u; i < page_count; i++) {
        const auto page = pmm::allocate();

        // TODO: Implement some sort of panic function
        if (page == 0) {
            utils::print_format("failed to allocate a page for heap segment #%d\n", i);

            while (true) {
                asm volatile("hlt");
            }
        }

        paging::map_page(virtual_base + i * 4096, page, PageTableEntryFlags::PRESENT | PageTableEntryFlags::WRITE);
    }

    const auto heap_size = page_count * 4096;
    const auto heap_block_count = heap_size / 64;

    m_heap_bitmap.initialize((u8*) virtual_base, heap_block_count);
    m_heap_bitmap.clear(false);
    m_heap_bitmap.set_range(0, 1, true);

    m_preferred_allocation = 0;
    m_total_memory = heap_size - 64;
    m_used_memory = 64;

    utils::print_format("initialized heap of size %dKiB at %x\n", m_total_memory / 1024, virtual_base);
}

u64 memory::heap::allocate(u64 size) {
    size = size < 64 ? 64 : align_up(size, 64);

    const auto block_count = size / 64;
    const auto index = m_heap_bitmap.find_range(m_preferred_allocation, block_count, false);

    // TODO: Implement some sort of panic function
    if (index == -1)
        return 0;

    m_heap_bitmap.set_range(index, block_count, true);
    m_used_memory += size;
    m_preferred_allocation = index + block_count;

    return m_heap_base + (u64) index * 64;
}

void memory::heap::free(u64 address, u64 size) {
    // TODO: Implement some sort of panic function
    if (address < m_heap_base || address >= m_heap_base + m_total_memory) {
        return;
    }

    size = size < 64 ? 64 : align_up(size, 64);

    const auto heap_offset = address - m_heap_base;
    const auto block_offset = heap_offset / 64;
    const auto block_count = size / 64;

    m_heap_bitmap.set_range(block_offset, block_count, false);
    m_used_memory -= size;
}

u64 memory::heap::get_total_memory() {
    return m_total_memory;
}

u64 memory::heap::get_free_memory() {
    return m_total_memory - m_used_memory;
}

u64 memory::heap::get_used_memory() {
    return m_used_memory;
}
