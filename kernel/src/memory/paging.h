#pragma once

#include <base/macros.h>
#include <base/types.h>

#include <boot/stivale.h>

enum class PageTableEntryFlags : u64
{
    NONE = 0,
    PRESENT = 1 << 0,
    WRITE = 1 << 1,
    USER = 1 << 2,
    WRITE_THROUGH = 1 << 3,
    NO_CACHE = 1 << 4,
    ACCESSED = 1 << 5,
    DIRTY = 1 << 6,
    GLOBAL = 1 << 8,
};

MAKE_ENUM_OPERATORS(PageTableEntryFlags);

class PageTableEntry {
public:
    constexpr PageTableEntry() = default;
    constexpr PageTableEntry(u64 address, PageTableEntryFlags flags) : value{address | (u64) flags} {}

    constexpr u64 address() const { return value & 0x000ffffffffff000; }
    constexpr PageTableEntryFlags flags() const { return (PageTableEntryFlags) (value & ~0x000ffffffffff000); }

    constexpr void set_address(u64 address) { value = (value & ~0x000ffffffffff000) | address; }
    constexpr void set_flags(PageTableEntryFlags flags) { value = (value & 0x000ffffffffff000) | (u64) flags; }

private:
    u64 value;
};

class PageTable {
public:
    PageTableEntry& get(usize index) { return m_entries[index]; }
    const PageTableEntry& get(usize index) const { return m_entries[index]; }

    PageTableEntry& operator[](usize index) { return get(index); }
    const PageTableEntry& operator[](usize index) const { return get(index); }

private:
    PageTableEntry m_entries[512];
};

namespace memory::paging {

void initialize(StivaleStructPmrsTag* pmrs);

void map_page(u64 virtual_address, u64 physical_address, PageTableEntryFlags flags);
void map_pages(u64 virtual_start, u64 physical_start, u64 count, PageTableEntryFlags flags);

void unmap_page(u64 virtual_address);
void unmap_pages(u64 virtual_start, u64 count);

}  // namespace memory::paging
