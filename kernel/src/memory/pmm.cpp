#include <base/memory.h>

#include <memory/bitmap.h>
#include <memory/pmm.h>

#include <utils/address.h>
#include <utils/print.h>

static Bitmap m_pmm_bitmap;
static usize m_preferred_allocation;
static u64 m_total_memory;
static u64 m_used_memory;

static u64 get_highest_mmap_address(StivaleStructMemoryMapTag *memory_map) {
    u64 highest_address = 0;

    for (auto i = 0u; i < memory_map->length; i++) {
        const auto &entry = memory_map->entries[i];

        if (entry.type != StivaleMemoryMapType::USABLE)
            continue;

        const auto end_address = align_up(entry.base + entry.size, 4096);

        if (end_address > highest_address)
            highest_address = end_address;
    }

    return highest_address;
}

static StringView get_entry_type_string(StivaleMemoryMapType type) {
    switch (type) {
        case StivaleMemoryMapType::USABLE:
            return "USABLE";
        case StivaleMemoryMapType::RESERVED:
            return "RESERVED";
        case StivaleMemoryMapType::ACPI_RECLAIMABLE:
            return "ACPI_RECLAIMABLE";
        case StivaleMemoryMapType::ACPI_NVS:
            return "ACPI_NVS";
        case StivaleMemoryMapType::BAD_MEMORY:
            return "BAD_MEMORY";
        case StivaleMemoryMapType::BOOTLOADER_RECLAIMABLE:
            return "BOOTLOADER_RECLAIMABLE";
        case StivaleMemoryMapType::KERNEL_AND_MODULES:
            return "KERNEL_AND_MODULES";
        case StivaleMemoryMapType::FRAMEBUFFER:
            return "FRAMEBUFFER";
        default:
            return "UNKNOWN";
    }
}

void memory::pmm::initialize(StivaleStructMemoryMapTag *memory_map) {
    utils::print("= current system memory map\n");

    for (auto i = 0; i < memory_map->length; i++) {
        const auto &entry = memory_map->entries[i];

        utils::print_format("== base=%x, length=%x, type=%s\n", entry.base, entry.size, get_entry_type_string(entry.type).data());
    }

    const auto highest_mmap_address = get_highest_mmap_address(memory_map);
    const auto target_size = highest_mmap_address / 4096 / 8 + 1;

    utils::print_format("required bitmap size is %dKiB\n", target_size / 1024);

    StivaleMemoryMapEntry *usable_entry = nullptr;

    for (auto i = 0; usable_entry == nullptr && i < memory_map->length; i++) {
        auto &entry = memory_map->entries[i];

        if (entry.type != StivaleMemoryMapType::USABLE)
            continue;

        if (entry.size >= target_size)
            usable_entry = &entry;
    }

    // TODO: Implement some sort of panic function
    if (usable_entry == nullptr) {
        utils::print("no usable memory map entry found\n");

        while (true) {
            asm volatile("hlt");
        }
    }

    const auto bitmap_base = align_down(usable_entry->base, 4096);

    utils::print_format("usable memory entry found at %x\n", bitmap_base);

    m_pmm_bitmap.initialize((u8 *) bitmap_base, target_size);
    m_pmm_bitmap.clear(true);

    m_preferred_allocation = 0;
    m_total_memory = 0;
    m_used_memory = 0;

    for (auto i = 0; i < memory_map->length; i++) {
        const auto &entry = memory_map->entries[i];
        const auto base = align_down(entry.base, 4096);
        const auto size = align_up(entry.size, 4096);

        if (entry.type != StivaleMemoryMapType::USABLE)
            continue;

        m_pmm_bitmap.set_range(base / 4096, size / 4096, false);
        m_total_memory += size;
    }

    m_pmm_bitmap.set_range(0, 32, true);
    m_pmm_bitmap.set_range(bitmap_base / 4096, align_up(target_size, 4096) / 4096, true);

    m_used_memory += 32 * 4096;
    m_used_memory += align_up(target_size, 4096);

    utils::print_format("memory available: %dKiB\n", m_total_memory / 1024);
}

u64 memory::pmm::allocate() {
    return allocate(1);
}

u64 memory::pmm::allocate(usize count) {
    const auto index = m_pmm_bitmap.find_range(m_preferred_allocation, count, false);

    // TODO: Implement some sort of panic function
    if (index == -1)
        return 0;

    m_pmm_bitmap.set_range(index, count, true);

    m_used_memory += count * 4096;
    m_preferred_allocation = index + count;

    const auto address = (u64) index * 4096;

    memory_set((u8 *) address, 0, count * 4096);

    return address;
}

void memory::pmm::free(u64 address) {
    free(address, 1);
}

void memory::pmm::free(u64 address, usize count) {
    // TODO: Make sure the range is valid and already allocated

    m_pmm_bitmap.set_range(address / 4096, count, false);
    m_used_memory -= count * 4096;
}

u64 memory::pmm::get_total_memory() {
    return m_total_memory;
}

u64 memory::pmm::get_free_memory() {
    return m_total_memory - m_used_memory;
}

u64 memory::pmm::get_used_memory() {
    return m_used_memory;
}
