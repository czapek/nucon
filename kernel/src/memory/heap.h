#pragma once

#include <base/types.h>

namespace memory::heap {

void initialize(u64 virtual_base, u64 page_count);

u64 allocate(u64 size);

void free(u64 address, u64 size);

u64 get_total_memory();
u64 get_free_memory();
u64 get_used_memory();

}  // namespace memory::heap
