#include <cpu/idt.h>

#include <utils/backtrace.h>
#include <utils/print.h>

static void __attribute__((noreturn)) hang() {
    while (true) {
        asm volatile("hlt");
    }
}

static void print_interrupt_info(IsrStackFrame *stack_frame) {
    utils::print_format("= isr stack frame:\n");
    utils::print_format("== cpu flags=%b\n", stack_frame->cpu_flags);
    utils::print_format("== fault rip=%x:%x\n", stack_frame->code_segment, stack_frame->rip);
    utils::print_format("== fault rsp=%x:%x\n", stack_frame->stack_segment, stack_frame->rsp);

    utils::print_backtrace();
}

void __attribute__((interrupt, noreturn)) unhandled_isr(IsrStackFrame *stack_frame) {
    utils::print("caught an unhandled interrupt\n");

    print_interrupt_info(stack_frame);

    hang();
}

void __attribute__((interrupt, noreturn)) invalid_opcode_isr(IsrStackFrame *stack_frame) {
    utils::print("tried to execute an invalid instruction\n");

    print_interrupt_info(stack_frame);

    hang();
}

void __attribute__((interrupt, noreturn)) double_fault_isr(IsrStackFrame *stack_frame) {
    utils::print("prevented a double fault\n");

    print_interrupt_info(stack_frame);

    hang();
}

void __attribute__((interrupt, noreturn)) gp_fault_isr(IsrStackFrame *stack_frame, u64 error_code) {
    utils::print("general protection fault\n");

    print_interrupt_info(stack_frame);

    hang();
}

void __attribute__((interrupt, noreturn)) page_fault_isr(IsrStackFrame *stack_frame, u64 error_code) {
    u64 faulty_address;

    asm volatile("movq %%cr2, %0" : "=r"(faulty_address));

    const auto write_attempt = (error_code & 1 << 1) != 0;
    const auto user_mode = (error_code & 1 << 2) != 0;
    const auto instruction_fetch = (error_code & 1 << 4) != 0;

    if (write_attempt) {
        utils::print_format("page fault at %x trying to write to %x\n", stack_frame->rip, faulty_address);
    } else if (instruction_fetch) {
        utils::print_format("page fault at %x trying to execute %x\n", stack_frame->rip, faulty_address);
    } else {
        utils::print_format("page fault at %x trying to read from %x\n", stack_frame->rip, faulty_address);
    }

    print_interrupt_info(stack_frame);

    hang();
}
