cmake_minimum_required(VERSION 3.16)

file(GLOB_RECURSE SOURCES "src/*.cpp")

add_executable(kernel ${SOURCES})
target_include_directories(kernel PRIVATE src)
target_link_libraries(kernel PRIVATE core elf gfx)
target_link_options(kernel PRIVATE "-T${CMAKE_SOURCE_DIR}/meta/kernel.ld")
